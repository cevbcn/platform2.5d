﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform targetToFollow;

    // Update is called once per frame
    void Update () {
        this.transform.position = new Vector3(targetToFollow.position.x,this.transform.position.y, this.transform.position.z);
    }
}

